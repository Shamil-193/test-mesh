import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EntityActionFactory, EntityOp, EntitySelectorsFactory } from '@ngrx/data';
import { Dictionary } from '@ngrx/entity';
import { Store } from '@ngrx/store';
import { Observable, Subject, takeUntil, tap } from 'rxjs';
import { Product, ProductEntity } from 'src/app/shared/interfaces/product';
import { ProductService } from '../shared/services/product.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePageComponent implements OnInit, OnDestroy {

  form: FormGroup = new FormGroup({})
  product$: Observable<Dictionary<Product>>
  protected componentDestroy$: Subject<void> = new Subject<void>();

  constructor(
    private productService: ProductService,
    protected readonly store$: Store<Product>,
    protected readonly entitySelectorsFactory: EntitySelectorsFactory,
    protected readonly entityActionFactory: EntityActionFactory,
  ) { }

  ngOnInit() {
    this.product$ = this.store$.select(this.entitySelectorsFactory.create<Product>(ProductEntity.Product).selectEntityMap)
    this.store$.dispatch(this.entityActionFactory.create(ProductEntity.Product, EntityOp.QUERY_ALL))
    this.formInit()
  }

  ngOnDestroy(): void {
    this.componentDestroy$.next();
    this.componentDestroy$.complete();
  }

  formInit(): void {
    this.product$.pipe(
      takeUntil(this.componentDestroy$),
      tap((productsEntities: Dictionary<Product>) => {
        Object.entries(this.form.value).forEach(([existingId]) => {
          if (!productsEntities[existingId]) {
            (this.form as FormGroup).removeControl(existingId)
          }
        })
        Object.entries(productsEntities).forEach(([newId, product]: [string, Product]) => {
          if (!this.form.value[newId]) {
            (this.form as FormGroup).addControl(product["id"], new FormControl(product.name, Validators.required))
          }
        })
      }
      )).subscribe()
  }

  updateCard(id: string): void {
    this.productService.update({ id, name: this.form.controls[id].value })
    // this.store$.dispatch(this.entityActionFactory.create(ProductEntity.Product, EntityOp.SAVE_UPDATE_ONE, { id, name: this.form.controls[id].value }))
  }

  deleteCard(id: string): void {
    this.store$.dispatch(this.entityActionFactory.create(ProductEntity.Product, EntityOp.REMOVE_ONE, id))
  }


  createCard(): void {
    this.store$.dispatch(this.entityActionFactory.create(ProductEntity.Product, EntityOp.SAVE_ADD_ONE, { id: (new Date()).getTime().toString(), name: "" }))
  }

}
