export interface Product {
    id: string;
    name: string;
}

export enum ProductEntity {
    Product = 'Product',
}