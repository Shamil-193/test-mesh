import { Injectable } from "@angular/core";
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from "@ngrx/data";
import { Product, ProductEntity } from "../interfaces/product";

@Injectable({ providedIn: 'root' })

export class ProductService extends EntityCollectionServiceBase<Product> {

  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(ProductEntity.Product, serviceElementsFactory);
  }
  
}