import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";

const ProductArr = [
    { name: 'Table', id: "0000000000000001"},
    { name: 'Plate', id: "0000000000000002"},
    { name: 'Apple', id: "0000000000000003"},
]

@Injectable()
export class BackendInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.method === "GET" && request.url === "api/products/") {
            return of(new HttpResponse({ status: 200, body: ProductArr }));
        } 
        if (request.method === "POST" && request.url === "api/product/") {
            return of(new HttpResponse({status: 200, body: {...request.body}}));
        } 
        if (request.method === "DELETE" && request.url === `api/product/${request.body.id}`) {
            return of(new HttpResponse({ ...request.body }));
        } 
        if (request.method === "PUT" && request.url === `api/product/${request.body.id}`) {
            return of(new HttpResponse({ ...request.body }));
        }
        return next.handle(request)
    }
}